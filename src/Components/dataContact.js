import React, {useState,  useEffect } from "react";
import axios from "axios";

const DataContact = ({idx, item}) => {
    const [del, setDel] = useState('')
    useEffect(() => {
        axios.get(`/service/getmnp?secret=H38oGo8fg82fg&phone=${item.Mobile}`, 
        {
            headers: {
                "Content-Type": "application/json"
            },
        })
        .then(res => {
            setDel(res.data);
        })
    },[])
    return(
        <tr key={idx}>
            <td>{idx + 1}</td>
            <td>{item.Name}</td>
            <td >{del === '' ? item.Mobile : ''}</td>
        </tr>
    )
   
}

export default DataContact