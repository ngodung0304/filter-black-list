import React from 'react'
import { CSVLink } from 'react-csv'
import ReactExport from 'react-data-export';

export const ExportReactCSV = ({csvData, filename}) => {
    return (
        <button variant="warning"  style={{marginTop:5+'px', border:'none'}}>
            <CSVLink data={csvData} filename={filename}><button type='submit' 
            className='btn btn-danger' style={{border:'none', backgroundColor:'red', color:'white'}}>Export</button> </CSVLink>
        </button>
    )
}