const {createProxyMiddleware} = require('http-proxy-middleware')

module.exports = app => {
    app.use(
        createProxyMiddleware('/service/getmnp',{
            target: 'http://site9.siptrunk.vn:62211',
            changeOrigin : true
        })
    )
}