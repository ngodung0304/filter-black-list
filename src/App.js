import { saveAs } from 'file-saver';
import {useState} from 'react'
import * as XLSX from 'xlsx'
import DataContact from './Components/dataContact'
import {ExportReactCSV} from './Components/exportData'
function App() {

  // on change states
  const [excelFile, setExcelFile]=useState(null);
  const [excelFileError, setExcelFileError]=useState(null);  
  const [name, setName] = useState('')
  // submit
  const [excelData, setExcelData]=useState([]);
  // it will contain array of objects
  const [newData, setNewData] = useState('')
  // handle File
  const fileType=['application/vnd.ms-excel'];
  const handleFile = (e)=>{
    let selectedFile = e.target.files[0];
    if(selectedFile){
      // console.log(selectedFile.type);
      if(selectedFile&&fileType.includes(selectedFile.type)){
        let reader = new FileReader();
        reader.readAsArrayBuffer(selectedFile);
        reader.onload=(e)=>{
          setExcelFileError(null);
          setExcelFile(e.target.result);
          setName(selectedFile.name);
        } 
      }
      else{
        setExcelFileError('Please select only excel file types');
        setExcelFile(null);
      }
    }
    else{
      console.log('plz select your file');
    }
  }
  // submit function
  const handleSubmit=(e)=>{
    e.preventDefault();
    
    if(excelFile!==null){
      const workbook = XLSX.read(excelFile,{type:'buffer'});
      const worksheetName = workbook.SheetNames[0];
      var worksheet=workbook.Sheets[worksheetName]
      const data = XLSX.utils.sheet_to_json(worksheet);
      setExcelData(data);
    }
    else{
      setExcelFileError('Please select files excel ');
    }
  }
  return (
    <div className="container">
      {/* upload file section */}
      <div className='form'>
        <div className='form-group' autoComplete="off"
        >
          <label><h5>Upload Excel file</h5></label>
          <br></br>
          <input type='file' name='file' className='form-control'
          onChange={handleFile} required></input>                  
          {excelFileError&&<div className='text-danger'
          style={{marginTop:5+'px'}}>{excelFileError}</div>}
          <button type='submit' className='btn btn-success'
          style={{marginTop:5+'px'}}  onClick={handleSubmit}>Submit</button>
        </div>
      </div>

      <br></br>
      <hr></hr>

      {/* view file section */}
      {excelData.length > 0 ? 
      <div>
        <h5>View Excel file</h5>
        {excelData.length > 0 ? <div className="col-md-4 center">
        <ExportReactCSV csvData={excelData} filename={name} /> 
      </div> : ''}
     
      <div className='viewer'>
        
        {excelData.length === 0 &&<>No file selected</>}
        {excelData.length > 0 &&(
          <div className='table-responsive'>
            <table className='table'>
              <thead>
                <tr style={{fontWeight:'bold'}}>
                  <th scope='col' >ID</th>
                  <th scope='col'> Name</th>
                  <th scope='col'> Mobile</th>   
                </tr>
              </thead>
              <tbody style={{display:'block'}}>
                {excelData.length > 0 && excelData?.map((item, idx) => {
                 return(
                  <DataContact item={item} idx={idx} />
                 )
                })}
              </tbody>
              
            </table>            
          </div>
        )}    
           
      </div>
      </div> : ''}
     
    </div>
  );
}

export default App;

